include .env

migrate:
	docker-compose run app python src/manage.py migrate $(if $m, api $m,)

makemigrations:
	python src/manage.py makemigrations && sudo chown -R ${USER} src/app/migrations/

createsuperuser:
	docker-compose run app python src/manage.py createsuperuser

collectstatic:
	docker-compose run app python src/manage.py collectstatic --no-input

dev:
	docker-compose run app python src/manage.py runserver localhost:8000

command:
	docker-compose run app python src/manage.py ${c}

shell:
	docker-compose run app python src/manage.py shell

debug:
	docker-compose run app python src/manage.py debug

piplock:
	pipenv install && sudo chown -R ${USER} Pipfile.lock

run_lint:
	isort .
	flake8 --config setup.cfg
	black --config pyproject.toml .

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --config pyproject.toml .

up:
	docker-compose up -d

down:
	docker-compose down

build:
	docker-compose build

push:
	docker push ${IMAGE_APP}

pull:
	docker pull ${IMAGE_APP}

lint:
	docker-compose run app isort --check --diff .
	docker-compose run app flake8 --config setup.cfg
	docker-compose run app black --check --config pyproject.toml .

test:
	docker-compose run app pytest src/tests

test_unit:
	docker-compose run app pytest src/tests/unit

test_integration:
	docker-compose run app pytest src/tests/integration

test_smoke:
	docker-compose run app pytest src/tests/smoke
