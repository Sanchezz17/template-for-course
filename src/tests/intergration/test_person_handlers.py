import pytest
from telegram.ext import DispatcherHandlerStop

from app.internal.models.person import Person
from app.internal.transport.bot.person_handlers import START_MESSAGE_TEXT, start

# @pytest.mark.django_db
# def test_start(test_person: Person, test_update, test_context):
#     with pytest.raises(DispatcherHandlerStop):
#         start(test_update, test_context)
#         test_context.bot.send_message.assert_called_once_with(
#             chat_id=test_update.effective_chat.id, text=START_MESSAGE_TEXT
#         )
