from unittest import mock

import pytest

from app.internal.models.person import Person


@pytest.fixture(scope="function")
def test_update(test_person: Person):
    update_mock = mock.MagicMock()
    update_mock.effective_user.id.return_value = test_person.telegram_id
    update_mock.effective_user.username.return_value = test_person.telegram_username
    update_mock.effective_user.full_name.return_value = test_person.telegram_name
    update_mock.effective_chat.id.return_value = 1
    return update_mock


@pytest.fixture(scope="function")
def test_context():
    context = mock.MagicMock()
    context.bot = mock.MagicMock()
    return context
