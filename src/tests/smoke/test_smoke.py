import json

import pytest
from django.http import HttpResponse
from django.urls import reverse

from app.internal.models.person import Person
from app.internal.services.jwt_token_service import generate_access_token, generate_refresh_token


@pytest.mark.django_db
def test_me_view_unauthorized(client, test_person: Person):
    url = reverse("me")
    response: HttpResponse = client.get(url)
    assert response.status_code == 401


@pytest.mark.django_db
def test_me_view(client, test_person: Person):
    url = reverse("me")
    access_token, expires_at = generate_access_token(telegram_id=test_person.telegram_id)
    response: HttpResponse = client.get(url, **{"HTTP_Authorization": f"Bearer {access_token}"})
    content = json.loads(response.content)
    assert content["telegram_id"] == test_person.telegram_id
    assert response.status_code == 200


@pytest.mark.django_db
def test_refresh_view(client, test_person: Person):
    url = reverse("refresh")
    refresh_token = generate_refresh_token(person=test_person)
    response: HttpResponse = client.post(
        url, data=json.dumps({"refresh_token": refresh_token}), content_type="application/json"
    )
    content = json.loads(response.content)
    assert response.status_code == 200
    assert content["access_token"] is not None
    assert content["expires_at"] is not None
    assert content["refresh_token"] is not None
