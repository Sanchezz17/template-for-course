from app.internal.services.phone_number_service import parse_phone_number


def test_parse_phone_number():
    assert parse_phone_number("89999999999") is None
    assert parse_phone_number("+79500706845") is not None
