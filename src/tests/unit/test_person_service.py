import phonenumbers
import pytest

from app.internal.models.person import Person
from app.internal.services.person_service import (
    create_or_update_person,
    get_person_by_telegram_id,
    get_person_by_telegram_username,
    set_phone_number_to_person,
)


@pytest.mark.django_db
def test_get_person_by_telegram_id(test_person: Person):
    actual = get_person_by_telegram_id(telegram_id=test_person.telegram_id)
    assert actual == test_person


@pytest.mark.django_db
def test_get_person_by_telegram_username(test_person: Person):
    actual = get_person_by_telegram_username(telegram_username=test_person.telegram_username)
    assert actual == test_person


@pytest.mark.django_db
def test_set_phone_number_to_person(test_person: Person):
    new_phone_number = phonenumbers.parse("+79637001485")
    set_phone_number_to_person(telegram_id=test_person.telegram_id, phone_number=new_phone_number)
    actual = get_person_by_telegram_id(telegram_id=test_person.telegram_id)
    assert actual.phone_number == new_phone_number


@pytest.mark.django_db
def test_create_or_update_person(test_person: Person):
    updated_person = create_or_update_person(
        telegram_id=test_person.telegram_id,
        telegram_username=test_person.telegram_username,
        telegram_name=test_person.telegram_name,
    )
    assert updated_person == test_person

    created_person = create_or_update_person(
        telegram_id=2, telegram_username="test username", telegram_name="test name"
    )
    assert created_person.telegram_id == 2
    assert created_person.telegram_username == "test username"
    assert created_person.telegram_name == "test name"
