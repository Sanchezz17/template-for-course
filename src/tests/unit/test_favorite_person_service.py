import pytest

from app.internal.models.favorite_person import FavoritePerson
from app.internal.models.person import Person
from app.internal.services.favorite_person_service import (
    add_favorite_person,
    get_favorite_persons_by_owner_telegram_id,
    remove_favorite_person_by_owner_telegram_id_and_favorite_telegram_username,
)


@pytest.mark.django_db
def test_get_favorite_persons_by_owner_telegram_id(test_favorite_person: FavoritePerson):
    actual = get_favorite_persons_by_owner_telegram_id(owner_telegram_id=test_favorite_person.owner.telegram_id)
    assert actual == [test_favorite_person]


@pytest.mark.django_db
def test_add_favorite_person(test_person: Person, test_another_person: Person):
    actual = add_favorite_person(owner=test_another_person, favorite=test_person)
    assert actual.owner == test_another_person
    assert actual.favorite == test_person


@pytest.mark.django_db
def test_remove_favorite_person_by_owner_telegram_id_and_favorite_telegram_username(
    test_favorite_person: FavoritePerson,
):
    deleted = remove_favorite_person_by_owner_telegram_id_and_favorite_telegram_username(
        owner_telegram_id=test_favorite_person.owner.telegram_id,
        favorite_telegram_username=test_favorite_person.favorite.telegram_username,
    )
    assert deleted == 1
