import pytest

from app.internal.models.person import Person
from app.internal.services.jwt_token_service import (
    JTI_FIELD,
    TELEGRAM_ID_FIELD,
    decode_token,
    generate_access_token,
    generate_refresh_token,
)


@pytest.mark.django_db
def test_generate_access_token(test_person: Person):
    access_token, expires_at = generate_access_token(telegram_id=test_person.telegram_id)
    payload = decode_token(access_token)
    assert payload[TELEGRAM_ID_FIELD] == test_person.telegram_id


@pytest.mark.django_db
def test_generate_refresh_token(test_person: Person):
    refresh_token = generate_refresh_token(person=test_person)
    payload = decode_token(refresh_token)
    assert len(payload[JTI_FIELD]) == 36
