import phonenumbers
import pytest
from djmoney.money import Money

from app.internal.models.bank_account import BankAccount
from app.internal.models.bank_transaction import BankTransaction
from app.internal.models.favorite_person import FavoritePerson
from app.internal.models.person import Person


@pytest.fixture(scope="function")
def test_another_person(
    telegram_id=2, telegram_username="another username", telegram_name="another person", phone_number="+79630307555"
):
    return Person.objects.create(
        telegram_id=telegram_id,
        telegram_username=telegram_username,
        telegram_name=telegram_name,
        phone_number=phonenumbers.parse(phone_number),
    )


@pytest.fixture(scope="function")
def test_another_bank_account(test_another_person: Person, number="5" * 20):
    return BankAccount.objects.create(number=number, balance=Money(20000, "RUB"), owner=test_another_person)


@pytest.fixture(scope="function")
def test_favorite_person(test_person: Person, test_another_person: Person):
    return FavoritePerson.objects.create(owner=test_person, favorite=test_another_person)


@pytest.fixture(scope="function")
def test_bank_transaction(test_bank_account: BankAccount, test_another_bank_account: BankAccount):
    return BankTransaction.objects.create(sender=test_bank_account, recipient=test_another_bank_account, amount=500)
