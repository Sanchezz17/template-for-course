import pytest

from app.internal.models.bank_card import BankCard
from app.internal.services.bank_card_service import get_bank_card_by_number


@pytest.mark.django_db
def test_get_bank_card_by_number(test_bank_card: BankCard):
    actual = get_bank_card_by_number(number=test_bank_card.number)
    assert actual == test_bank_card
