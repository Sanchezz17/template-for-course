import pytest

from app.internal.models.bank_transaction import BankTransaction
from app.internal.models.person import Person
from app.internal.services.bank_transaction_service import (
    get_bank_transactions_by_number,
    get_interacted_with_usernames,
)


@pytest.mark.django_db
def test_get_bank_transactions_by_number(test_bank_transaction: BankTransaction):
    actual = get_bank_transactions_by_number(number=test_bank_transaction.sender.number)
    assert list(actual) == [test_bank_transaction]


# @pytest.mark.django_db
# def test_get_interacted_with_usernames(test_person: Person, test_another_person: Person):
#     actual = get_interacted_with_usernames(telegram_username=test_person.telegram_username)
#     assert actual == [test_another_person.telegram_username]
