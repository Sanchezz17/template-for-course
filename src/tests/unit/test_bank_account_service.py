from decimal import Decimal

import pytest

from app.internal.models.bank_account import BankAccount
from app.internal.services.bank_account_service import (
    get_bank_account_by_number,
    get_bank_account_by_number_or_username,
    transfer_money_between_accounts,
)


@pytest.mark.django_db
def test_get_bank_account_by_number(test_bank_account: BankAccount):
    actual = get_bank_account_by_number(number=test_bank_account.number)
    assert actual == test_bank_account


@pytest.mark.django_db
def test_get_bank_account_by_number_or_username(test_bank_account: BankAccount):
    actual = get_bank_account_by_number_or_username(number_or_username=test_bank_account.owner.telegram_username)
    assert actual == test_bank_account


@pytest.mark.django_db
def test_transfer_money_between_accounts(test_bank_account: BankAccount, test_another_bank_account: BankAccount):
    transfer_amount = Decimal(1000)
    initial_sender_amount = test_bank_account.balance.amount
    initial_recipient_amount = test_another_bank_account.balance.amount
    transfer_money_between_accounts(test_bank_account, test_another_bank_account, transfer_amount)
    assert test_bank_account.balance.amount == initial_sender_amount - transfer_amount
    assert test_another_bank_account.balance.amount == initial_recipient_amount + transfer_amount
