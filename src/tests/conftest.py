import phonenumbers
import pytest
from djmoney.money import Money

from app.internal.models.bank_account import BankAccount
from app.internal.models.bank_card import BankCard
from app.internal.models.person import Person


@pytest.fixture(scope="function")
def test_person(telegram_id=1, telegram_username="username", telegram_name="person", phone_number="+79530407656"):
    return Person.objects.create(
        telegram_id=telegram_id,
        telegram_username=telegram_username,
        telegram_name=telegram_name,
        phone_number=phonenumbers.parse(phone_number),
    )


@pytest.fixture(scope="function")
def test_bank_account(test_person: Person, number="7" * 20):
    return BankAccount.objects.create(number=number, balance=Money(500000, "RUB"), owner=test_person)


@pytest.fixture(scope="function")
def test_bank_card(test_bank_account: BankAccount, number="7" * 16):
    return BankCard.objects.create(number=number, account=test_bank_account)
