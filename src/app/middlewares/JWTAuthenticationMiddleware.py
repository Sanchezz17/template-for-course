from datetime import datetime

from django.http import HttpRequest, HttpResponse

from app.internal.services.jwt_token_service import decode_token
from app.internal.services.person_service import get_person_by_telegram_id

AUTHORIZATION_HEADER = "authorization"
BEARER = "bearer"
EXPIRES_AT_FIELD = "expires_at"
TELEGRAM_ID_FIELD = "telegram_id"


class JWTAuthenticationMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_view(self, request: HttpRequest, view_func, view_args, view_kwargs):
        if request.user.is_superuser or (
            request.path.startswith("/api/auth/") and not request.path.endswith("logout/")
        ):
            return None

        try:
            authorization_header = request.headers.get(AUTHORIZATION_HEADER).split()
            if len(authorization_header) == 2 and authorization_header[0].lower() == BEARER:
                access_token = authorization_header[1]
                payload = decode_token(access_token)
                expires_at = payload[EXPIRES_AT_FIELD]
                if expires_at < datetime.utcnow().timestamp():
                    return HttpResponse("401 Unauthorized. Access token expired", status=401)
                person = get_person_by_telegram_id(telegram_id=payload[TELEGRAM_ID_FIELD])
                request.person = person
            else:
                return HttpResponse(
                    "401 Unauthorized. Specify access token. Example: Bearer {access_token}", status=401
                )
        except:
            return HttpResponse("401 Unauthorized", status=401)

        return None
