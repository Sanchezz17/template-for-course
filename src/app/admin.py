from django.contrib import admin

from app.internal.admin.admin_user import AdminUserAdmin
from app.internal.admin.bank_account import BankAccountAdmin
from app.internal.admin.bank_card import BankCardAdmin
from app.internal.admin.bank_transaction import BankTransaction
from app.internal.admin.favorite_person import FavoritePersonAdmin
from app.internal.admin.issued_token import IssuedTokenAdmin
from app.internal.admin.person import PersonAdmin

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
