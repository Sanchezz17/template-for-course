# Generated by Django 4.0.2 on 2022-05-19 17:45

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("app", "0008_alter_person_password_hash"),
    ]

    operations = [
        migrations.AlterField(
            model_name="issuedtoken",
            name="person",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, related_name="refresh_tokens", to="app.person"
            ),
        ),
    ]
