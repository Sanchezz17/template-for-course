from django.http import HttpRequest, HttpResponse
from django.views import View

from app.internal.services.jwt_token_service import remove_refresh_token_by_person


class LogoutView(View):
    def post(self, request: HttpRequest):
        person = request.person
        remove_refresh_token_by_person(person)
        return HttpResponse(status=200)
