import json
from datetime import datetime

from django.http import HttpRequest, HttpResponse, JsonResponse
from django.views import View

from app.internal.services.jwt_token_service import (
    JTI_FIELD,
    REFRESH_TOKEN_EXPIRES_RELATED_TO_NOW,
    decode_token,
    generate_access_token,
    generate_refresh_token,
    get_refresh_token_by_jti,
    remove_refresh_token_by_person_and_device_id,
)

REFRESH_TOKEN_FIELD = "refresh_token"


class RefreshView(View):
    def post(self, request: HttpRequest):
        content = json.loads(request.body)
        refresh_token = content[REFRESH_TOKEN_FIELD]

        payload = decode_token(refresh_token)
        issued_token = get_refresh_token_by_jti(payload[JTI_FIELD])

        if not issued_token:
            return HttpResponse("401 Unauthorized", status=401)

        if (issued_token.created_at + REFRESH_TOKEN_EXPIRES_RELATED_TO_NOW).timestamp() < datetime.utcnow().timestamp():
            return HttpResponse("401 Unauthorized. Refresh token expired", status=401)

        person = issued_token.person
        access_token, expires_at = generate_access_token(telegram_id=person.telegram_id)
        remove_refresh_token_by_person_and_device_id(person=issued_token.person, device_id=issued_token.device_id)
        refresh_token = generate_refresh_token(person=person)
        return JsonResponse({"access_token": access_token, "expires_at": expires_at, "refresh_token": refresh_token})
