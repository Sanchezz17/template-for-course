import json

from django.http import HttpRequest, HttpResponse, HttpResponseForbidden, JsonResponse
from django.views import View

from app.internal.services.jwt_token_service import generate_access_token, generate_refresh_token
from app.internal.services.password_service import encrypt_password
from app.internal.services.person_service import get_person_by_telegram_username

LOGIN_FIELD = "login"
PASSWORD_FIELD = "password"


class LoginView(View):
    def post(self, request: HttpRequest):
        content = json.loads(request.body)
        login = content[LOGIN_FIELD]
        person = get_person_by_telegram_username(telegram_username=login)
        if not person:
            return HttpResponse("401 Unauthorized", status=401)

        password = content[PASSWORD_FIELD]
        password_hash = encrypt_password(password=password)
        if bytes(person.password_hash) != password_hash:
            return HttpResponseForbidden()

        access_token, expires_at = generate_access_token(telegram_id=person.telegram_id)
        refresh_token = generate_refresh_token(person=person)
        return JsonResponse({"access_token": access_token, "expires_at": expires_at, "refresh_token": refresh_token})
