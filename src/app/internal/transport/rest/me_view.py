from django.http import JsonResponse
from django.views import View


class MeView(View):
    def get(self, request):
        person = request.person
        return JsonResponse(
            {
                "telegram_id": person.telegram_id,
                "telegram_username": person.telegram_username,
                "telegram_name": person.telegram_name,
                "phone_number": str(person.phone_number),
            }
        )
