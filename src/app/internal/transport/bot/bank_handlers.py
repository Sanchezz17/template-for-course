from decimal import Decimal

from telegram import Update
from telegram.ext import CallbackContext, CommandHandler

from app.internal.services.bank_account_service import (
    get_bank_account_by_number,
    get_bank_account_by_number_or_username,
    transfer_money_between_accounts,
)
from app.internal.services.bank_card_service import get_bank_card_by_number
from app.internal.services.bank_transaction_service import (
    get_bank_transactions_by_number,
    get_interacted_with_usernames,
)

INVALID_USAGE_MESSAGE_TEXT = "Неверное использование команды"

ACCOUNT_BALANCE_HELP_TEXT = "Попробуйте /account_balance {номер счёта}"

BANK_ACCOUNT_NOT_FOUND_OR_FORBIDDEN_MESSAGE_TEXT = "Счёт не существует или вы не имеете к нему доступ"

BANK_ACCOUNT_BALANCE_MESSAGE_TEXT = "Баланс счёта: "

CARD_BALANCE_HELP_TEXT = "Попробуйте /card_balance {номер карты}"

BANK_CARD_NOT_FOUND_OR_FORBIDDEN_MESSAGE_TEXT = "Карта не существует или вы не имеете к ней доступ"

BANK_CARD_BALANCE_MESSAGE_TEXT = "Баланс карты: "

TRANSFER_MONEY_HELP_TEXT = (
    "Попробуйте /transfer_money {номер карты/счета отправителя} "
    "{номер карты/счета/username в telegram получателя} {сумма перевода}"
)

SENDER_BANK_ACCOUNT_NOT_FOUND_OR_FORBIDDEN_MESSAGE_TEXT = (
    "Счёт отправителя не существует" " или вы не имеете к нему доступ"
)

RECIPIENT_BANK_ACCOUNT_NOT_FOUND_MESSAGE_TEXT = "Счёт получателя не найден"

SENDER_BANK_ACCOUNT_IS_RECIPIENT_BANK_ACCOUNT = "Счёт получателя должен отличаться от счета отправителя"

CURRENCIES_NOT_MATCH_MESSAGE_TEXT = "Валюта счета отправителя и валюта счета получателя не совпадают"

TRANSFER_AMOUNT_INVALID_MESSAGE_TEXT = "Сумма перевода должна быть числом"

INSUFFICIENT_FUNDS_MESSAGE_TEXT = "Недостаточно средств"

TRANSFER_COMPLETED_MESSAGE_TEXT = "Перевод успешно выполнен"

TRANSACTIONS_HELP_TEXT = "Попробуйте /transactions {номер карты/счета} "

TRANSACTIONS_MESSAGE_TEXT = "Выписка по счету/карте"

EMPTY_TRANSACTIONS_MESSAGE_TEXT = "В выписке нет ни одной операции"

INTERACTED_WITH_MESSAGE_TEXT = "Пользователи, с которыми когда-либо было взаимодействие:\n\n"

EMPTY_INTERACTED_WITH_MESSAGE_TEXT = "Вы не взаимодействовали ни с одним из пользователей"


def account_balance(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    if not context.args or len(context.args) > 1:
        context.bot.send_message(chat_id=chat_id, text=f"{INVALID_USAGE_MESSAGE_TEXT}\n{ACCOUNT_BALANCE_HELP_TEXT}")
        return

    bank_account_number = context.args[0]
    bank_account = get_bank_account_by_number(number=bank_account_number)

    if bank_account is None or bank_account.owner.telegram_id != update.effective_user.id:
        context.bot.send_message(chat_id=chat_id, text=BANK_ACCOUNT_NOT_FOUND_OR_FORBIDDEN_MESSAGE_TEXT)
        return

    balance_text = f"{BANK_ACCOUNT_BALANCE_MESSAGE_TEXT}{bank_account.balance}"
    context.bot.send_message(chat_id=chat_id, text=balance_text)


def card_balance(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    if not context.args or len(context.args) > 1:
        context.bot.send_message(chat_id=chat_id, text=f"{INVALID_USAGE_MESSAGE_TEXT}\n{CARD_BALANCE_HELP_TEXT}")
        return

    bank_card_number = context.args[0]
    bank_card = get_bank_card_by_number(number=bank_card_number)

    if bank_card is None or bank_card.account.owner.telegram_id != update.effective_user.id:
        context.bot.send_message(chat_id=chat_id, text=BANK_CARD_NOT_FOUND_OR_FORBIDDEN_MESSAGE_TEXT)
        return

    balance_text = f"{BANK_CARD_BALANCE_MESSAGE_TEXT}{bank_card.account.balance}"
    context.bot.send_message(chat_id=chat_id, text=balance_text)


def transfer_money(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    user_id = update.effective_user.id
    if not context.args or len(context.args) != 3:
        context.bot.send_message(chat_id=chat_id, text=f"{INVALID_USAGE_MESSAGE_TEXT}\n{TRANSFER_MONEY_HELP_TEXT}")
        return

    sender_number = context.args[0]
    sender_bank_account = get_bank_account_by_number_or_username(number_or_username=sender_number)

    if sender_bank_account is None or sender_bank_account.owner.telegram_id != user_id:
        context.bot.send_message(chat_id=chat_id, text=SENDER_BANK_ACCOUNT_NOT_FOUND_OR_FORBIDDEN_MESSAGE_TEXT)
        return

    recipient_number_or_username = context.args[1]
    recipient_bank_account = get_bank_account_by_number_or_username(number_or_username=recipient_number_or_username)

    if recipient_bank_account is None:
        context.bot.send_message(chat_id=chat_id, text=RECIPIENT_BANK_ACCOUNT_NOT_FOUND_MESSAGE_TEXT)
        return

    if sender_bank_account.id == recipient_bank_account.id:
        context.bot.send_message(chat_id=chat_id, text=SENDER_BANK_ACCOUNT_IS_RECIPIENT_BANK_ACCOUNT)
        return

    if sender_bank_account.balance.currency != recipient_bank_account.balance.currency:
        context.bot.send_message(chat_id=chat_id, text=CURRENCIES_NOT_MATCH_MESSAGE_TEXT)
        return

    transfer_amount = try_parse_decimal(context.args[2])
    if transfer_amount is None:
        context.bot.send_message(chat_id=chat_id, text=TRANSFER_AMOUNT_INVALID_MESSAGE_TEXT)
        return

    rest_amount = sender_bank_account.balance.amount - transfer_amount
    if rest_amount < 0:
        context.bot.send_message(chat_id=chat_id, text=INSUFFICIENT_FUNDS_MESSAGE_TEXT)
        return

    transfer_money_between_accounts(sender_bank_account, recipient_bank_account, transfer_amount)
    context.bot.send_message(chat_id=chat_id, text=TRANSFER_COMPLETED_MESSAGE_TEXT)


def try_parse_decimal(s):
    try:
        return Decimal(s)
    except ValueError:
        return None


def transactions(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    if not context.args or len(context.args) != 1:
        context.bot.send_message(chat_id=chat_id, text=f"{INVALID_USAGE_MESSAGE_TEXT}\n{TRANSACTIONS_HELP_TEXT}")
        return

    number = context.args[0]

    bank_account = get_bank_account_by_number_or_username(number_or_username=number)
    if bank_account is None or bank_account.owner.telegram_id != update.effective_user.id:
        context.bot.send_message(chat_id=chat_id, text=BANK_ACCOUNT_NOT_FOUND_OR_FORBIDDEN_MESSAGE_TEXT)
        return

    bank_transactions = get_bank_transactions_by_number(number=number)

    bank_transactions_message_text = f"{TRANSACTIONS_MESSAGE_TEXT} {number}\n\n"
    for index, bank_transaction in enumerate(bank_transactions):
        bank_transactions_message_text += (
            f"{index + 1}. "
            f"Дата: {bank_transaction.occurred_at}. \n"
            f"Отправитель: @{bank_transaction.sender.owner.telegram_username}. "
            f"Счёт: {bank_transaction.sender.number}. \n"
            f"Получатель: @{bank_transaction.recipient.owner.telegram_username}. "
            f"Счёт: {bank_transaction.recipient.number}. \n"
            f"Сумма: {bank_transaction.amount} {bank_transaction.sender.balance.currency}.\n\n"
        )
    if not bank_transactions:
        bank_transactions_message_text = EMPTY_TRANSACTIONS_MESSAGE_TEXT
    context.bot.send_message(chat_id=update.effective_chat.id, text=bank_transactions_message_text)


def interacted_with(update: Update, context: CallbackContext):
    interacted_with_usernames = get_interacted_with_usernames(telegram_username=update.effective_user.username)
    interacted_with_message_text = INTERACTED_WITH_MESSAGE_TEXT
    for index, interacted_with_username in enumerate(interacted_with_usernames):
        interacted_with_message_text += f"{index + 1}. @{interacted_with_username}\n"
    if not interacted_with_usernames:
        interacted_with_message_text = EMPTY_INTERACTED_WITH_MESSAGE_TEXT
    context.bot.send_message(chat_id=update.effective_chat.id, text=interacted_with_message_text)


ACCOUNT_BALANCE_COMMAND = "account_balance"
account_balance_command_handler = CommandHandler(ACCOUNT_BALANCE_COMMAND, account_balance)

CARD_BALANCE_COMMAND = "card_balance"
card_balance_command_handler = CommandHandler(CARD_BALANCE_COMMAND, card_balance)

TRANSFER_MONEY_COMMAND = "transfer_money"
transfer_money_command_handler = CommandHandler(TRANSFER_MONEY_COMMAND, transfer_money)

TRANSACTIONS_COMMAND = "transactions"
transactions_command_handler = CommandHandler(TRANSACTIONS_COMMAND, transactions)

INTERACTED_WITH_COMMAND = "interacted_with"
interacted_with_command_handler = CommandHandler(INTERACTED_WITH_COMMAND, interacted_with)
