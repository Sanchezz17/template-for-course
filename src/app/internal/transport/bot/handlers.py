from app.internal.transport.bot.bank_handlers import (
    account_balance_command_handler,
    card_balance_command_handler,
    interacted_with_command_handler,
    transactions_command_handler,
    transfer_money_command_handler,
)
from app.internal.transport.bot.favorite_person_handlers import (
    add_favorite_command_handler,
    favorite_command_handler,
    remove_favorite_command_handler,
)
from app.internal.transport.bot.person_handlers import (
    check_person_handler,
    check_person_phone_number_handler,
    me_command_handler,
    set_password_command_handler,
    set_phone_command_handler,
    start_command_handler,
)

START_HANDLERS_GROUP = 0
SET_PHONE_NUMBER_HANDLERS_GROUP = 1
AUTHORIZED_HANDLERS_GROUP = 2

bot_handlers = {
    START_HANDLERS_GROUP: [start_command_handler, check_person_handler],
    SET_PHONE_NUMBER_HANDLERS_GROUP: [set_phone_command_handler, check_person_phone_number_handler],
    AUTHORIZED_HANDLERS_GROUP: [
        set_password_command_handler,
        me_command_handler,
        account_balance_command_handler,
        card_balance_command_handler,
        transfer_money_command_handler,
        favorite_command_handler,
        add_favorite_command_handler,
        remove_favorite_command_handler,
        transactions_command_handler,
        interacted_with_command_handler,
    ],
}
