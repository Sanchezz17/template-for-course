from telegram import Update
from telegram.ext import CallbackContext, CommandHandler

from app.internal.services.favorite_person_service import (
    add_favorite_person,
    get_favorite_persons_by_owner_telegram_id,
    remove_favorite_person_by_owner_telegram_id_and_favorite_telegram_username,
)
from app.internal.services.person_service import get_person_by_telegram_username

FAVORITE_MESSAGE_TEXT = "Пользователи в избранном:\n\n"

EMPTY_FAVORITE_MESSAGE_TEXT = "В избранном нет пользователей"

INVALID_USAGE_MESSAGE_TEXT = "Неверное использование команды"

ADD_FAVORITE_HELP_TEXT = "Попробуйте /add_favorite {username}"

FAVORITE_ADDED_MESSAGE_TEXT = "Пользователь добавлен в избранное"

REMOVE_FAVORITE_HELP_TEXT = "Попробуйте /remove_favorite {username}"

FAVORITE_REMOVED_MESSAGE_TEXT = "Пользователь удалён из избранного"

FAILED_REMOVE_FAVORITE_MESSAGE_TEXT = (
    "Не удалось удалить пользователя из избранного. " "Пользователь не был в избранном"
)


def get_favorite(update: Update, context: CallbackContext):
    favorite_persons = get_favorite_persons_by_owner_telegram_id(owner_telegram_id=update.effective_user.id)
    favorite_persons_message_text = FAVORITE_MESSAGE_TEXT
    for index, favorite_person in enumerate(favorite_persons):
        favorite_persons_message_text += f"{index + 1}. @{favorite_person.favorite.telegram_username}\n"
    if not favorite_persons:
        favorite_persons_message_text = EMPTY_FAVORITE_MESSAGE_TEXT
    context.bot.send_message(chat_id=update.effective_chat.id, text=favorite_persons_message_text)


def add_favorite(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    if not context.args or len(context.args) > 1:
        context.bot.send_message(chat_id=chat_id, text=f"{INVALID_USAGE_MESSAGE_TEXT}\n{ADD_FAVORITE_HELP_TEXT}")
        return

    favorite_telegram_username = context.args[0]
    favorite = get_person_by_telegram_username(favorite_telegram_username)
    if favorite is None:
        context.bot.send_message(chat_id=chat_id, text=f"Пользователь {favorite_telegram_username} не зарегистрирован")

    owner = get_person_by_telegram_username(update.effective_user.username)
    add_favorite_person(owner, favorite)
    context.bot.send_message(chat_id=chat_id, text=FAVORITE_ADDED_MESSAGE_TEXT)


def remove_favorite(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    if not context.args or len(context.args) > 1:
        context.bot.send_message(chat_id=chat_id, text=f"{INVALID_USAGE_MESSAGE_TEXT}\n{REMOVE_FAVORITE_HELP_TEXT}")
        return

    favorite_telegram_username = context.args[0]
    favorite = get_person_by_telegram_username(favorite_telegram_username)
    if favorite is None:
        context.bot.send_message(chat_id=chat_id, text=f"Пользователь {favorite_telegram_username} не зарегистрирован")

    deleted = remove_favorite_person_by_owner_telegram_id_and_favorite_telegram_username(
        owner_telegram_id=update.effective_user.id, favorite_telegram_username=favorite.telegram_username
    )
    if deleted > 0:
        context.bot.send_message(chat_id=chat_id, text=FAVORITE_REMOVED_MESSAGE_TEXT)
    else:
        context.bot.send_message(chat_id=chat_id, text=FAILED_REMOVE_FAVORITE_MESSAGE_TEXT)


FAVORITE_COMMAND = "favorite"
favorite_command_handler = CommandHandler(FAVORITE_COMMAND, get_favorite)

ADD_FAVORITE_COMMAND = "add_favorite"
add_favorite_command_handler = CommandHandler(ADD_FAVORITE_COMMAND, add_favorite)

REMOVE_FAVORITE_COMMAND = "remove_favorite"
remove_favorite_command_handler = CommandHandler(REMOVE_FAVORITE_COMMAND, remove_favorite)
