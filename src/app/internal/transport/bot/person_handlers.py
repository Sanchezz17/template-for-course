from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, DispatcherHandlerStop, Filters, MessageHandler

from app.internal.services.jwt_token_service import generate_refresh_token
from app.internal.services.password_service import encrypt_password
from app.internal.services.person_service import (
    create_or_update_person,
    exists_person_by_telegram_id,
    get_person_by_telegram_id,
    get_person_phone_number_by_telegram_id,
    print_person_info,
    set_password_hash_to_person,
    set_phone_number_to_person,
)
from app.internal.services.phone_number_service import parse_phone_number

START_MESSAGE_TEXT = (
    "Привет! Информация о вас сохранена.\nПожалуйста укажите номер теелфона с помощью "
    "команды /set_phone номер_телефона."
)

INVALID_PHONE_NUMBER_MESSAGE_TEXT = "Номер телефона не указан или указаное значение не является номером телефона"

PHONE_NUMBER_SAVED_MESSAGE_TEXT = "Номер телефона сохранён"

UNREGISTERED_PERSON_MESSAGE_TEXT = "Вы не зарегистрированы. Для начала работы с ботом выполните команду /start"

PHONE_NUMBER_NOT_PROVIDED_MESSAGE_TEXT = (
    "Вы не указали номер телефона. Пожалуйста укажите номер теелфона с помощью " "команды /set_phone номер_телефона"
)

PASSWORD_SAVED_MESSAGE_TEXT = "Пароль сохранён"

INVALID_USAGE_MESSAGE_TEXT = "Неверное использование команды"

SET_PASSWORD_HELP_TEXT = "Попробуйте /set_password {пароль}"


def start(update: Update, context: CallbackContext):
    telegram_user = update.effective_user
    create_or_update_person(
        telegram_id=telegram_user.id, telegram_username=telegram_user.username, telegram_name=telegram_user.full_name
    )
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=START_MESSAGE_TEXT,
    )
    raise DispatcherHandlerStop


def set_phone(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    phone_number_str = " ".join(context.args)
    phone_number = parse_phone_number(phone_number_str=phone_number_str)
    if phone_number is None:
        context.bot.send_message(chat_id=chat_id, text=INVALID_PHONE_NUMBER_MESSAGE_TEXT)
        return

    set_phone_number_to_person(telegram_id=update.effective_user.id, phone_number=phone_number)
    context.bot.send_message(chat_id=chat_id, text=PHONE_NUMBER_SAVED_MESSAGE_TEXT)


def check_person(update: Update, context: CallbackContext):
    if not exists_person_by_telegram_id(telegram_id=update.effective_user.id):
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=UNREGISTERED_PERSON_MESSAGE_TEXT,
        )
        raise DispatcherHandlerStop


def check_person_phone_number(update: Update, context: CallbackContext):
    phone_number = get_person_phone_number_by_telegram_id(telegram_id=update.effective_user.id)
    if not phone_number:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=PHONE_NUMBER_NOT_PROVIDED_MESSAGE_TEXT,
        )
        raise DispatcherHandlerStop


def set_password(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    if not context.args or len(context.args) > 1:
        context.bot.send_message(chat_id=chat_id, text=f"{INVALID_USAGE_MESSAGE_TEXT}\n{SET_PASSWORD_HELP_TEXT}")
        return
    password_hash = encrypt_password(context.args[0])
    set_password_hash_to_person(telegram_id=update.effective_user.id, password_hash=password_hash)
    context.bot.send_message(chat_id=chat_id, text=PASSWORD_SAVED_MESSAGE_TEXT)


def me(update: Update, context: CallbackContext):
    person = get_person_by_telegram_id(telegram_id=update.effective_user.id)
    person_info_text = print_person_info(person=person)
    context.bot.send_message(chat_id=update.effective_chat.id, text=person_info_text)


START_COMMAND = "start"
start_command_handler = CommandHandler(START_COMMAND, start)
check_person_handler = MessageHandler(Filters.command, check_person)

SET_PHONE_COMMAND = "set_phone"
set_phone_command_handler = CommandHandler(SET_PHONE_COMMAND, set_phone)
check_person_phone_number_handler = MessageHandler(Filters.command, check_person_phone_number)

SET_PASSWORD_COMMAND = "set_password"
set_password_command_handler = CommandHandler(SET_PASSWORD_COMMAND, set_password)
ME_COMMAND = "me"
me_command_handler = CommandHandler(ME_COMMAND, me)
