from telegram.ext import Updater

from app.internal.transport.bot.handlers import bot_handlers
from config.settings import HOST, TELEGRAM_BOT_PORT, TELEGRAM_BOT_TOKEN


def run_bot():
    updater = Updater(token=TELEGRAM_BOT_TOKEN, use_context=True)
    dispatcher = updater.dispatcher
    for bot_handlers_group in bot_handlers:
        for handler in bot_handlers[bot_handlers_group]:
            dispatcher.add_handler(handler, bot_handlers_group)
    updater.start_webhook(
        listen="0.0.0.0",
        port=TELEGRAM_BOT_PORT,
        url_path=TELEGRAM_BOT_TOKEN,
        webhook_url=f"https://{HOST}/{TELEGRAM_BOT_TOKEN}",
    )
    updater.idle()
