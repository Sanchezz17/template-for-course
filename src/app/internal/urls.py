from django.urls import path

from app.internal.transport.rest.login_view import LoginView
from app.internal.transport.rest.logout_view import LogoutView
from app.internal.transport.rest.me_view import MeView
from app.internal.transport.rest.refresh_view import RefreshView

urlpatterns = [
    path("auth/login/", LoginView.as_view(), name="login"),
    path("auth/refresh/", RefreshView.as_view(), name="refresh"),
    path("auth/logout/", LogoutView.as_view(), name="logout"),
    path("me/", MeView.as_view(), name="me"),
]
