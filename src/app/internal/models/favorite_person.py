from django.db import models

from app.internal.models.person import Person


class FavoritePerson(models.Model):
    owner = models.ForeignKey(Person, related_name="owner_favorite_person_set", on_delete=models.CASCADE)
    favorite = models.ForeignKey(Person, related_name="favorite_favorite_person_set", on_delete=models.CASCADE)

    def __str__(self):
        return (
            f"Owner username: {self.owner.telegram_username}\n"
            f"Favorite username: {self.favorite.telegram_username}\n"
        )

    class Meta:
        verbose_name = "Favorite person"
        verbose_name_plural = "Favorite people"
