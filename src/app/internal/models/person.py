from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class Person(models.Model):
    telegram_id = models.IntegerField()
    telegram_username = models.CharField(max_length=255)
    telegram_name = models.CharField(max_length=255)
    phone_number = PhoneNumberField(blank=True)
    password_hash = models.BinaryField(editable=True, null=True, max_length=65000)

    def __str__(self):
        return (
            f"Telegram id: {self.telegram_id}\n"
            f"Telegram username: {self.telegram_username}\n"
            f"Telegram name: {self.telegram_name}\n"
            f"Номер телефона: {self.phone_number}"
        )

    class Meta:
        verbose_name = "Person"
        verbose_name_plural = "People"
