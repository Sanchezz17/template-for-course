import random

from django.core.validators import RegexValidator
from django.db import models
from djmoney.models.fields import MoneyField

from app.internal.models.person import Person

NUMBER_MAX_LENGTH = 20
NUMBER_VALIDATOR_REGEX = rf"\d{{{NUMBER_MAX_LENGTH}}}"
NUMBER_VALIDATOR_MESSAGE = f"Номер банковского счета должен состоять из {NUMBER_MAX_LENGTH} цифр"

number_validator = RegexValidator(regex=NUMBER_VALIDATOR_REGEX, message=NUMBER_VALIDATOR_MESSAGE)


def random_bank_account_number():
    return str(random.randint(10 ** (NUMBER_MAX_LENGTH - 1), 10**NUMBER_MAX_LENGTH - 1))


class BankAccount(models.Model):
    number = models.CharField(
        max_length=NUMBER_MAX_LENGTH, validators=[number_validator], default=random_bank_account_number, unique=True
    )
    balance = MoneyField(max_digits=14, decimal_places=2, default_currency="RUB")
    owner = models.ForeignKey(Person, on_delete=models.CASCADE)

    def __str__(self):
        return f"Number: {self.number}\n" f"Balance: {self.balance}\n" f"Owner: {self.owner}\n"
