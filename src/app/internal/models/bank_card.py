import random

from django.core.validators import RegexValidator
from django.db import models

from app.internal.models.bank_account import BankAccount

NUMBER_MAX_LENGTH = 16
NUMBER_VALIDATOR_REGEX = rf"\d{{{NUMBER_MAX_LENGTH}}}"
NUMBER_VALIDATOR_MESSAGE = f"Номер банковской карты должен состоять из {NUMBER_MAX_LENGTH} цифр"

number_validator = RegexValidator(regex=NUMBER_VALIDATOR_REGEX, message=NUMBER_VALIDATOR_MESSAGE)


def random_bank_card_number():
    return str(random.randint(10 ** (NUMBER_MAX_LENGTH - 1), 10**NUMBER_MAX_LENGTH - 1))


class BankCard(models.Model):
    number = models.CharField(
        max_length=NUMBER_MAX_LENGTH, validators=[number_validator], default=random_bank_card_number, unique=True
    )
    account = models.ForeignKey(BankAccount, on_delete=models.CASCADE)

    def __str__(self):
        return f"Number: {self.number}\n" f"Account: {self.account}\n"
