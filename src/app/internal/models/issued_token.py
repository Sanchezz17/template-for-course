import uuid

from django.db import models
from django.utils import timezone

from app.internal.models.person import Person


class IssuedToken(models.Model):
    jti = models.CharField(max_length=255, primary_key=True)
    person = models.ForeignKey(Person, on_delete=models.CASCADE, related_name="refresh_tokens")
    created_at = models.DateTimeField(default=timezone.now)
    revoked = models.BooleanField(default=False)
    device_id = models.UUIDField(default=uuid.uuid4, unique=True)
