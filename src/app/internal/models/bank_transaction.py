from django.db import models
from django.utils import timezone

from app.internal.models.bank_account import BankAccount


class BankTransaction(models.Model):
    sender = models.ForeignKey(BankAccount, related_name="sender_bank_transaction_set", on_delete=models.CASCADE)
    recipient = models.ForeignKey(BankAccount, related_name="recipient_bank_transaction_set", on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=14, decimal_places=2)
    occurred_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return (
            f"Sender: {self.sender.number}, {self.sender.owner.telegram_username}\n"
            f"Recipient: {self.recipient.number}, {self.recipient.owner.telegram_username}\n"
            f"Amount: {self.amount}\n"
        )
