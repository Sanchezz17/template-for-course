from phonenumbers import PhoneNumber

from app.internal.models.person import Person


def exists_person_by_telegram_id(telegram_id: int) -> Person | None:
    return Person.objects.filter(telegram_id=telegram_id).exists()


def get_person_by_telegram_id(telegram_id: int) -> Person | None:
    return Person.objects.filter(telegram_id=telegram_id).first()


def get_person_phone_number_by_telegram_id(telegram_id: int) -> PhoneNumber | None:
    return Person.objects.filter(telegram_id=telegram_id).values_list("phone_number", flat=True).first()


def get_person_by_telegram_username(telegram_username: str) -> Person | None:
    return Person.objects.filter(telegram_username=telegram_username).first()


def set_phone_number_to_person(telegram_id: int, phone_number: PhoneNumber) -> None:
    Person.objects.filter(telegram_id=telegram_id).update(phone_number=phone_number)


def set_password_hash_to_person(telegram_id: int, password_hash: bytes) -> None:
    Person.objects.filter(telegram_id=telegram_id).update(password_hash=password_hash)


def create_or_update_person(telegram_id: int, telegram_username: str, telegram_name: str) -> Person:
    person, _ = Person.objects.update_or_create(
        telegram_id=telegram_id, defaults={"telegram_username": telegram_username, "telegram_name": telegram_name}
    )
    return person


def print_person_info(person: Person) -> str:
    return f"{person}"
