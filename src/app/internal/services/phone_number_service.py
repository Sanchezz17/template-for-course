import phonenumbers
from phonenumbers import NumberParseException, PhoneNumber


def parse_phone_number(phone_number_str: str) -> PhoneNumber | None:
    try:
        phone_number = phonenumbers.parse(phone_number_str)
        is_valid_phone_number = phonenumbers.is_valid_number(phone_number)
        if not is_valid_phone_number:
            return None
        return phone_number
    except NumberParseException:
        return None
