from itertools import chain
from typing import List

from django.db.models import Q

from app.internal.models.bank_transaction import BankTransaction


def get_bank_transactions_by_number(number: str) -> List[BankTransaction]:
    return (
        BankTransaction.objects.select_related()
        .filter(
            Q(sender__number=number)
            | Q(recipient__number=number)
            | Q(sender__bankcard__number=number)
            | Q(recipient__bankcard__number=number)
        )
        .distinct()
    )


def get_interacted_with_usernames(telegram_username: str) -> List[str]:
    usernames = (
        BankTransaction.objects.filter(
            Q(sender__owner__telegram_username=telegram_username)
            | Q(recipient__owner__telegram_username=telegram_username)
        )
        .distinct()
        .values_list("sender__owner__telegram_username", "recipient__owner__telegram_username")
    )
    return list(set(chain.from_iterable(usernames)) - {telegram_username})
