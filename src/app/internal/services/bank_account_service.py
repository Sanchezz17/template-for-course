from decimal import Decimal

from django.db import transaction
from django.db.models import Q

from app.internal.models.bank_account import BankAccount
from app.internal.models.bank_transaction import BankTransaction


def get_bank_account_by_number(number: str) -> BankAccount | None:
    return BankAccount.objects.select_related().filter(number=number).first()


def get_bank_account_by_number_or_username(number_or_username: str) -> BankAccount | None:
    return (
        BankAccount.objects.select_related()
        .filter(
            Q(number=number_or_username)
            | Q(bankcard__number=number_or_username)
            | Q(owner__telegram_username=number_or_username)
        )
        .first()
    )


def transfer_money_between_accounts(
    sender_account: BankAccount, recipient_account: BankAccount, transfer_amount: Decimal
):
    with transaction.atomic():
        sender_account.balance.amount -= transfer_amount
        recipient_account.balance.amount += transfer_amount
        sender_account.save()
        recipient_account.save()
        BankTransaction.objects.create(sender=sender_account, recipient=recipient_account, amount=transfer_amount)
