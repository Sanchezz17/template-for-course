import uuid
from datetime import datetime, timedelta
from typing import Any

import jwt

from app.internal.models.issued_token import IssuedToken
from app.internal.models.person import Person
from config.settings import JWT_SECRET

ENCODE_ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRES_RELATED_TO_NOW = timedelta(minutes=5)
REFRESH_TOKEN_EXPIRES_RELATED_TO_NOW = timedelta(days=30)
EXPIRES_AT_FIELD = "expires_at"
TELEGRAM_ID_FIELD = "telegram_id"
JTI_FIELD = "jti"


def generate_access_token(telegram_id: int) -> (str, datetime):
    expires_at = (datetime.utcnow() + ACCESS_TOKEN_EXPIRES_RELATED_TO_NOW).timestamp()
    access_token = jwt.encode(
        payload={TELEGRAM_ID_FIELD: telegram_id, EXPIRES_AT_FIELD: expires_at},
        key=JWT_SECRET,
        algorithm=ENCODE_ALGORITHM,
    )
    return access_token, expires_at


def generate_refresh_token(person: Person) -> str:
    refresh_token = IssuedToken.objects.create(jti=uuid.uuid4(), person=person)
    return jwt.encode(payload={JTI_FIELD: str(refresh_token.jti)}, key=JWT_SECRET, algorithm=ENCODE_ALGORITHM)


def get_refresh_token_by_jti(jti: str) -> IssuedToken | None:
    return IssuedToken.objects.select_related("person").filter(jti=jti).first()


def remove_refresh_token_by_person(person: Person) -> int:
    deleted, _ = IssuedToken.objects.filter(person=person).delete()
    return deleted


def remove_refresh_token_by_person_and_device_id(person: Person, device_id: uuid.UUID) -> int:
    deleted, _ = IssuedToken.objects.filter(person=person, device_id=device_id).delete()
    return deleted


def decode_token(token: str) -> dict[str, Any]:
    return jwt.decode(token, key=JWT_SECRET, algorithms=[ENCODE_ALGORITHM])
