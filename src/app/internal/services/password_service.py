import hashlib

from config.settings import PASSWORD_SALT

HASH_NAME = "sha256"
ENCODING_NAME = "utf-8"
ITERATIONS_COUNT = 100000


def encrypt_password(password: str) -> bytes:
    return hashlib.pbkdf2_hmac(
        HASH_NAME, password.encode(ENCODING_NAME), PASSWORD_SALT.encode(ENCODING_NAME), ITERATIONS_COUNT
    )
