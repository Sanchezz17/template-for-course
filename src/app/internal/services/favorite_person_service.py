from typing import List

from app.internal.models.favorite_person import FavoritePerson
from app.internal.models.person import Person


def get_favorite_persons_by_owner_telegram_id(owner_telegram_id: int) -> List[FavoritePerson]:
    return list(FavoritePerson.objects.select_related().filter(owner__telegram_id=owner_telegram_id))


def add_favorite_person(owner: Person, favorite: Person) -> FavoritePerson:
    favorite_person, _ = FavoritePerson.objects.update_or_create(owner=owner, favorite=favorite)
    return favorite_person


def remove_favorite_person_by_owner_telegram_id_and_favorite_telegram_username(
    owner_telegram_id: int, favorite_telegram_username: str
) -> int:
    deleted, _ = FavoritePerson.objects.filter(
        owner__telegram_id=owner_telegram_id, favorite__telegram_username=favorite_telegram_username
    ).delete()
    return deleted
