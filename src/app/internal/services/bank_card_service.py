from app.internal.models.bank_card import BankCard


def get_bank_card_by_number(number: str) -> BankCard | None:
    return BankCard.objects.select_related().filter(number=number).first()
