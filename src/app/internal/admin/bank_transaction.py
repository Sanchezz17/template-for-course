from django.contrib import admin

from app.internal.models.bank_transaction import BankTransaction


@admin.register(BankTransaction)
class BankTransactionAdmin(admin.ModelAdmin):
    fields = ["sender", "recipient", "amount", "occurred_at"]
