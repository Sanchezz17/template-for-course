from django.contrib import admin

from app.internal.models.favorite_person import FavoritePerson


@admin.register(FavoritePerson)
class FavoritePersonAdmin(admin.ModelAdmin):
    fields = ["owner", "favorite"]
