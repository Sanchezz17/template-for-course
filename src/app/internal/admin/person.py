from django.contrib import admin

from app.internal.models.person import Person


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    fields = ["telegram_id", "telegram_username", "telegram_name", "phone_number", "password_hash"]
