from django.contrib import admin

from app.internal.models.issued_token import IssuedToken


@admin.register(IssuedToken)
class IssuedTokenAdmin(admin.ModelAdmin):
    fields = ["jti", "person", "created_at", "revoked", "device_id"]
