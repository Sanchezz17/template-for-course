FROM python:3.10-slim-buster

ENV PYTHONBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.9.0/wait /wait
RUN chmod +x /wait

RUN pip install pipenv

RUN apt-get update && \
    apt-get install -y --no-install-recommends gcc

RUN mkdir /app
WORKDIR /app

COPY Pipfile Pipfile.lock /app/

RUN pipenv install --deploy --system

COPY . /app/